import React from 'react';

export interface ButtonProps {
  GoOnClick?: () => any;
  StopOnClick?: () => any;
}
export const RedirectButtons: React.FC<ButtonProps> = ({
  GoOnClick,
  StopOnClick,
}) => {
  return (
    <div>
      <button onClick={GoOnClick} className="btn btn-primary display-4">
        <strong>Go!</strong>
      </button>
      <button onClick={StopOnClick} className="btn btn-secondary display-4">
        <strong>Stop!</strong>
      </button>
    </div>
  );
};
