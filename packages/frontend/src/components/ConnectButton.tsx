import React from 'react';

export interface ButtonProps {
  onClick?: () => any;
  connect: boolean;
}
export const ConnectButton: React.FC<ButtonProps> = ({ onClick, connect }) => {
  return (
    <div className="navbar-buttons mbr-section-btn">
      <button
        className={
          'btn btn-sm ' +
          (connect ? 'btn-primary ' : 'btn-secondary ') +
          'display-4'
        }
        onClick={onClick}
      >
        <span className="mobi-mbri mobi-mbri-unlock mbr-iconfont mbr-iconfont-btn"></span>
        {connect ? 'Connect a wallet' : 'Disconnect'}
      </button>
    </div>
  );
};
