import React from 'react';
import { AtokenContract } from '@jarvis/sponsoring-pool-blockchain';
import { RedirectButtons } from './RedirectButtons';

export const Asset: React.FC<{ contract: AtokenContract }> = ({ contract }) => {
  const name = contract.erc20Symbol;
  const logoUrl = `assets/images/${name.toLowerCase()}_logo.png`;
  return (
    <div className="card p-3 col-12 col-md-6 col-lg-4">
      <div className="card-wrapper">
        <div className="card-img">
          <img src={logoUrl} alt={`${contract.erc20Symbol} logo`} />
        </div>
        <div className="card-box">
          <p className="card-title mbr-fonts-style display-7">
            Redirect the interest earned on your {contract.erc20Symbol}{' '}
            deposited in Aave:
          </p>
        </div>
        <RedirectButtons
          GoOnClick={async () => await contract.redirect()}
          StopOnClick={async () => await contract.stopRedirect()}
        />
      </div>
    </div>
  );
};
