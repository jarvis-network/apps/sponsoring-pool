import React, { ReactNode } from 'react';

export const RowSection: React.FC<{
  className: string;
  elements: ReactNode[];
  rowSize: number;
}> = ({ elements, rowSize, className }) => (
  <section className={className}>
    <div className="container">
      {chunkArray(elements, rowSize).map(rowOfElements => (
        <div className="media-container-row">{rowOfElements}</div>
      ))}
    </div>
  </section>
);

function chunkArray<T>(array: T[], chunkSize: number) {
  const result = [];
  for (let i = 0; i < array.length; i += chunkSize) {
    result.push(array.slice(i, i + chunkSize));
  }
  return result;
}
