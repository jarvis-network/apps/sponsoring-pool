import { useEffect } from 'react';
import Web3 from 'web3';
import Notify from 'bnc-notify';

function memo<T>(create: () => T) {
  let result: T | null = null;
  return () => {
    if (!result) {
      result = create();
    }
    return result;
  };
}

const getNotify = memo(() =>
  Notify({
    dappId: process.env.REACT_APP_ONBOARD_KEY!,
    system: 'ethereum',
    networkId: parseInt(process.env.REACT_APP_NETWORK_ID!),
    darkMode: true,
    notifyMessages: {
      en: {
        transaction: {},
        watched: {
          txSent: 'Your transaction has been sent to the network',
          txPool: 'Your transaction is pending',
          txConfirmed: 'Your transaction has been confirmed',
          txFailed: 'Your transaction has been rejected',
        },
      },
    },
  }),
);

export function useNotification(
  web3: Web3 | null,
  pollingInterval = 1000,
): void {
  useEffect(() => {
    let account: string | null = null;
    async function getAccount() {
      if (web3 == null) {
        return;
      }
      const newAccount = (await web3.eth.getAccounts())[0];
      if (newAccount !== account) {
        if (account !== null) {
          getNotify().unsubscribe(account);
        }
        getNotify().account(newAccount);
        account = newAccount;
      }
    }
    const polling = setInterval(getAccount, pollingInterval);
    return () => {
      clearInterval(polling);
    };
  }, [web3, pollingInterval]);
}
