import Web3 from 'web3';
import Onboard from 'bnc-onboard';
import { useState } from 'react';

export function useWeb3() {
  const [web3, setWeb3] = useState<Web3 | null>(null);

  const fortmatic_key =
    process.env.REACT_APP_NETWORK_ID === '1'
      ? process.env.REACT_APP_FORTMATIC_MAINNET_KEY
      : process.env.REACT_APP_FORTMATIC_TESTNET_KEY;

  const onboard = Onboard({
    dappId: process.env.REACT_APP_ONBOARD_KEY,
    networkId: parseInt(process.env.REACT_APP_NETWORK_ID!),
    walletSelect: {
      heading: 'Select a wallet to connect to Jarvis Sponsoring Pool',
      description:
        'To use the Sponsoring Pool you need an Ethereum wallet. Please select yours from the list below:',
      wallets: [
        { walletName: 'metamask' },
        {
          walletName: 'walletConnect',
          infuraKey: process.env.REACT_APP_INFURA_KEY,
        },
        { walletName: 'unilogin' },
        {
          walletName: 'portis',
          apiKey: process.env.REACT_APP_PORTIS_KEY,
        },
        {
          walletName: 'fortmatic',
          apiKey: fortmatic_key,
        },
        {
          walletName: 'squarelink',
          apiKey: process.env.REACT_APP_SQUARELINK_KEY,
        },
        { walletName: 'dapper' },
        { walletName: 'authereum' },
        { walletName: 'trust' },
        { walletName: 'opera' },
        { walletName: 'coinbase' },
        { walletName: 'operaTouch' },
        { walletName: 'status' },
        { walletName: 'torus' },
        { walletName: 'walletLink', rpcUrl: process.env.REACT_APP_INFURA_KEY },
        {
          walletName: 'ledger',
          rpcUrl: process.env.REACT_APP_INFURA_KEY,
        },
      ],
    },
  });

  async function connectProvider() {
    if (!(await onboard.walletSelect()) || !(await onboard.walletCheck())) {
      if (web3) setWeb3(null);
      return;
    }
    const { wallet } = onboard.getState();
    const web3Instance = new Web3(wallet.provider);
    setWeb3(web3Instance);
  }

  async function disconnectProvider() {
    await onboard.walletReset();
    setWeb3(null);
  }

  return { web3, connectProvider, disconnectProvider };
}
