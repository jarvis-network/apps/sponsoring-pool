import React from 'react';
import './index.css';
import { HamburgerButton } from './components/HamburgerButton';
import { ConnectButton } from './components/ConnectButton';
import { useWeb3 } from './hooks/UseWeb3';
import { useNotification } from './hooks/UseNotification';
import { AtokenContract } from '@jarvis/sponsoring-pool-blockchain';
import { RowSection } from './components/RowSection';
import { Asset } from './components/Asset';
import Web3 from 'web3';

const AssetsSection: React.FC<{
  supportedAssets: string[];
  web3: Web3 | null;
}> = ({ supportedAssets, web3 }) => {
  return (
    <RowSection
      className="features3 cid-rVKVZDnrEr"
      rowSize={3}
      elements={supportedAssets.map(asset => (
        <Asset contract={new AtokenContract(web3!, asset)} />
      ))}
    />
  );
};

const joinText = (list: string[]) =>
  list.length > 1
    ? `${list.slice(0, list.length - 1).join(', ')} or ${list[list.length - 1]}`
    : `${list[0]}`;

const App: React.FC<{}> = () => {
  const { web3, connectProvider, disconnectProvider } = useWeb3();
  useNotification(web3);
  const supportedAssets = ['DAI', 'USDT', 'USDC', 'SUSD', 'BUSD', 'TUSD'];

  return (
    <div>
      <section className="menu cid-rVKVZCwMR4" id="menu1-10">
        <nav className="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-toggleable-sm bg-color transparent">
          <div className="menu-logo">
            <div className="navbar-brand">
              <span className="navbar-logo">
                <a href="https://staking.jarvis.network/en">
                  <img
                    src="assets/images/jarvis-logo-png-122x98.png"
                    alt="jarvis logo"
                    title=""
                    style={{ height: '3.8rem' }}
                  />
                </a>
              </span>
            </div>
          </div>
          <div id="navbarSupportedContent">
            <ConnectButton
              connect={!web3}
              onClick={async () =>
                web3 ? await disconnectProvider() : await connectProvider()
              }
            />
          </div>
        </nav>
      </section>
      <section className="mbr-section content4 cid-rVKVZBDmGW" id="content4-z">
        <div className="container">
          <div className="media-container-row">
            <div className="title col-12 col-md-8">
              <h2 className="align-center pb-3 mbr-fonts-style display-2">
                <strong>Sponsoring pool</strong>
              </h2>
              <h3 className="mbr-section-subtitle align-center mbr-light mbr-fonts-style display-5">
                <br />
                Redirect your stablecoin interest toward Jarvis to help us
                subsidize the transaction fees and receive up to 1% per week
                paid in JRT.
                <br />
                <br />
                <br />
                1. Deposit your stablecoin in Aave{' '}
                <strong>using this link</strong>:
              </h3>
              <div className="mbr-section-btn align-center py-4">
                <a
                  className="btn btn-primary display-4"
                  href="https://app.aave.com/?referral=33"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  Deposit funds in Aave
                </a>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className="mbr-section content4 cid-rVKZyS3TCK" id="content4-14">
        <div className="container">
          <div className="media-container-row">
            <div className="title col-12 col-md-8">
              <h3 className="mbr-section-subtitle align-center mbr-light mbr-fonts-style display-5">
                2. Click on these buttons to redirect the interest of your{' '}
                {joinText(supportedAssets)}:
              </h3>
            </div>
          </div>
        </div>
      </section>
      <AssetsSection supportedAssets={supportedAssets} web3={web3} />
    </div>
  );
};

export default App;
