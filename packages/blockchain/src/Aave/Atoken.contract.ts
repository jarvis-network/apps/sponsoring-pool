import Web3 from 'web3';
import { AbiItem } from 'web3-utils';
import { AToken } from '../contracts/AToken';
import { parse } from 'path';
const AtokenJSON = require('../../src/abi/AToken.json');
const REDIRECT_ADDRESS: string = '0x5ae7E199EC6ACfe1D7ee28401637a0AE897818c1';
interface tokenAddress {
  networkId: number;
  erc20Symbol: string;
  address: string;
}
const addressMap: tokenAddress[] = [
  {
    networkId: 1,
    erc20Symbol: 'DAI',
    address: '0xfC1E690f61EFd961294b3e1Ce3313fBD8aa4f85d',
  },
  {
    networkId: 3,
    erc20Symbol: 'DAI',
    address: '0xcB1Fe6F440c49E9290c3eb7f158534c2dC374201',
  },
  {
    networkId: 42,
    erc20Symbol: 'DAI',
    address: '0x58AD4cB396411B691A9AAb6F74545b2C5217FE6a',
  },
  {
    networkId: 1,
    erc20Symbol: 'USDT',
    address: '0x71fc860F7D3A592A4a98740e39dB31d25db65ae8',
  },
  {
    networkId: 3,
    erc20Symbol: 'USDT',
    address: '0x790744bC4257B4a0519a3C5649Ac1d16DDaFAE0D',
  },
  {
    networkId: 42,
    erc20Symbol: 'USDT',
    address: '0xA01bA9fB493b851F4Ac5093A324CB081A909C34B',
  },
  {
    networkId: 1,
    erc20Symbol: 'USDC',
    address: '0x9ba00d6856a4edf4665bca2c2309936572473b7e',
  },
  {
    networkId: 3,
    erc20Symbol: 'USDC',
    address: '0x2dB6a31f973Ec26F5e17895f0741BB5965d5Ae15',
  },
  {
    networkId: 42,
    erc20Symbol: 'USDC',
    address: '0x02F626c6ccb6D2ebC071c068DC1f02Bf5693416a',
  },
  {
    networkId: 1,
    erc20Symbol: 'SUSD',
    address: '0x625ae63000f46200499120b906716420bd059240',
  },
  {
    networkId: 3,
    erc20Symbol: 'SUSD',
    address: '0x5D17e0ea2d886F865E40176D71dbc0b59a54d8c1',
  },
  {
    networkId: 42,
    erc20Symbol: 'SUSD',
    address: '0xb9c1434ab6d5811d1d0e92e8266a37ae8328e901',
  },
  {
    networkId: 1,
    erc20Symbol: 'BUSD',
    address: '0x6Ee0f7BB50a54AB5253dA0667B0Dc2ee526C30a8',
  },
  {
    networkId: 3,
    erc20Symbol: 'BUSD',
    address: '0x81E065164bAC7203c3bFEB1a749F48a64383c6eE',
  },
  {
    networkId: 1,
    erc20Symbol: 'TUSD',
    address: '0x4da9b813057d04baef4e5800e36083717b4a0341',
  },
  {
    networkId: 3,
    erc20Symbol: 'TUSD',
    address: '0x82F01c5694f36690a985F01dC0aD46e1B20E7a1a',
  },
  {
    networkId: 42,
    erc20Symbol: 'TUSD',
    address: '0xA79383e0d2925527ba5Ec1c1bcaA13c28EE00314',
  },
];

export class AtokenContract {
  public readonly web3: Web3;
  public readonly abi: AbiItem[];
  public readonly erc20Symbol: string;
  public contract: AToken | null = null;
  constructor(web3Provider: Web3, erc20Symbol: string) {
    this.abi = AtokenJSON.abi as AbiItem[];
    this.web3 = web3Provider;
    this.erc20Symbol = erc20Symbol;
  }

  getContract(networkId: number) {
    if (!this.contract) {
      const addressToken = addressMap.filter(
        tokenInfo =>
          tokenInfo.networkId === networkId &&
          tokenInfo.erc20Symbol === this.erc20Symbol,
      );
      this.contract = new this.web3.eth.Contract(
        this.abi,
        addressToken[0].address,
      );
    }
    return this.contract;
  }

  async getAccount() {
    const accounts = await this.web3.eth.getAccounts();
    const account = accounts[0];
    return account;
  }

  async redirect() {
    if (!this.web3) return;
    const networkId = await this.web3.eth.net.getId();
    const account = await this.getAccount();
    await this.getContract(networkId)
      .methods.redirectInterestStream(REDIRECT_ADDRESS)
      .send({ from: account });
  }

  async stopRedirect() {
    if (!this.web3) return;
    const networkId = await this.web3.eth.net.getId();
    const account = await this.getAccount();
    await this.getContract(networkId)
      .methods.redirectInterestStream(account)
      .send({ from: account });
  }
}
