/* eslint-disable @typescript-eslint/camelcase */
/* Uncommenting the defaults below
provides for an easier quick-start with Ganache.
You can also follow this format for other networks;
see <http://truffleframework.com/docs/advanced/configuration>
for more details on how to specify configuration options!
*/

const HDWalletProvider = require('truffle-hdwallet-provider');
const infuraApikey = '7feb421412a143508867b650f6209592';
const mnemonic =
  'wonder glare also army glimpse hold this twist gaze energy page century';

module.exports = {
  mocha: {
    enableTimeouts: false,
  },
  plugins: ['truffle-security'],
  networks: {
    development: {
      host: '127.0.0.1',
      port: 8545,
      network_id: 5777,
      gas: 1000000,
      gasPrice: 1000000000,
    },
    /* ropsten: {
        host: '127.0.0.1',
        port: 8545,
        network_id: 3,
        gas:1000000,
        gasPrice: 20000000000,
      }, */
    ropsten_infura: {
      provider: () =>
        new HDWalletProvider(
          mnemonic,
          'https://ropsten.infura.io/v3/' + infuraApikey,
          0,
          10,
        ),
      network_id: 3,
      gas: 300000,
      gasPrice: 60000000000,
    },
    kovan_infura: {
      provider: () =>
        new HDWalletProvider(
          mnemonic,
          'https://kovan.infura.io/v3/' + infuraApikey,
          0,
          10,
        ),
      network_id: 42,
      gas: 300000,
      gasPrice: 60000000000,
    },
    rinkeby_infura: {
      provider: () =>
        new HDWalletProvider(
          mnemonic,
          'https://rinkeby.infura.io/v3/' + infuraApikey,
          0,
          10,
        ),
      network_id: 4,
      gas: 300000,
      gasPrice: 60000000000,
    },
    /* ganache: {
        host: '127.0.0.1',
        port: 7545,
        network_id: 619,
        gas:1000000,
        gasPrice: 20000000000,
      } */
  },
  compilers: {
    solc: {
      version: '0.5.12',
      settings: {
        optimizer: {
          enabled: true,
        },
      },
    },
  },
};
